let app = {
    // ----------------------------------------------------------------------------------------------------------------
    // MANIPULATION DU DOM DE L'APPLICATION
    // ----------------------------------------------------------------------------------------------------------------
    dom: {
        renderTemplateCopies: function (templateSelector, targetSelector, values, callback) 
        {
            const fragment = document.createDocumentFragment();
            const eventTemplate = document.querySelector(templateSelector).content;

            values.forEach(value => {
                const copy = document.importNode(eventTemplate, true);
                callback(copy, value);
                fragment.appendChild(copy);
            });

            const target = document.querySelector(targetSelector);

            while(target.firstChild) 
            {
                target.removeChild(target.firstChild);
            }

            target.appendChild(fragment);
        }
    },


    // ----------------------------------------------------------------------------------------------------------------
    // ARCHITECTURE MVC DE L'APPLICATION
    // ----------------------------------------------------------------------------------------------------------------
    mvc: {
        router: null,

        dispatchRoute(controllerInstance)
        {
            // vérifier instance du controller

            if (!controllerInstance.hasOwnProperty('url') || !controllerInstance.executeHttpRequest)
            {
                throw new Error(`Le controlleur ${controllerInstance.constructor.name} est invalide.`)
            }
            //récupérer this.url et faire une requête avec fetch

            fetch(controllerInstance.url)
                .then(response => response.text())
                .then(htmlContent => {
                    //Injecter le html récupérer dans la <main class='container>
                    document.querySelector('main.container').innerHTML = htmlContent;

                    //exécuter la méthode executeHttpRequest
                    controllerInstance.executeHttpRequest();
                })
            
            
        }
    }
};


// L'application est exportée afin d'être accessible par d'autres modules.
export default app;