import EventModel from "../modeles/EventModel.js";
import app from "../app/app.js";

class Search
{

    constructor()
    {
        this.url = 'views/search.html';
    }

    executeHttpRequest()
    {
        
        const eventModel = new EventModel();
        const form = document.getElementById('formSearch');

        form.addEventListener('submit', (event) => {
            event.preventDefault();

            let q = document.getElementById('q').value;

            eventModel.listAllEvents(q)
                    .then(data => {
                        app.dom.renderTemplateCopies('#event-template', '.event-list', data, (copy, evenement) => {
                            copy.querySelector('.event-title').textContent = evenement.title;
                            copy.querySelector('.event-image').src = evenement.image;
                            copy.querySelector('.event-description').textContent = evenement.description.slice(0,150)+'[...]';   
                        });
                        
                    });      
       });
       
    }


}

export default Search;