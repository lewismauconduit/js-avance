class EventModel
{
    constructor()
    {
        this.url = `https://opendata.paris.fr/api/records/1.0/search/?dataset=que-faire-a-paris-&facet=category&facet=tags&facet=address_zipcode&facet=address_city&facet=pmr&facet=blind&facet=deaf&facet=access_type&facet=price_type&q=`
    }

    listAllEvents(q = '')
    {
        return fetch(`${this.url}${encodeURIComponent(q)}`)
                        .then(response => response.json())
                        .then(openData => {
                            
                            let events = openData.records
                                    .map(record => record.fields)
                                    .map(field => {
                                        return {
                                            title: field.title,
                                            image: field.cover_url,
                                            description: field.description,
                                        };
                                    });
                            return events;
                        });
    }


}

export default EventModel;