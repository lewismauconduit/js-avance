'use strict';

/***********EXO 2*********/
/*
    var promise = new Promise(function (fulfill, reject) {
        setTimeout(() => fulfill('FULFILLED!'), 300);
        
    });
    
    promise.then(fulfill => console.log(fulfill));
*/
/***********EXO 3*********/
/*
var promise = new Promise(function (fulfill, reject) {
    setTimeout(() => reject(new Error('REJECTED!')), 300);
  });
  
  function onReject (error) {
    console.log(error.message);
  }
  
  promise.then(
      fulfill => null,
      reject => onReject(reject));
*/
/***********EXO 4*********/
/*

  var promise = new Promise(function (fulfill, reject) {
    fulfill('I FIRED');
    reject(new Error('I DID NOT FIRE'));
  });

  promise.then(fulfill => console.log(fulfill),
                reject => console.log(error));
*/
/***********EXO 5*********/
/*
var promise = new Promise(function (fulfill, reject) {
    fulfill('PROMISE VALUE');
});

promise.then(fulfill => console.log(fulfill));

console.log('MAIN PROGRAM');

*/
/***********EXO 6*********/
/*
var promise = Promise.reject(new Error('une erreur'));
promise.catch((error) => console.log(error.message));
*/
/***********EXO 7*********/
/*
first()
  .then(secret => second(secret))
  .then(finalValue => console.log(finalValue))
*/
/***********EXO 8*********/
/*
function attachTitle(name)
{
  return `DR. ${name}`;
}

var promise = Promise.resolve(`MANHATTAN`).then(attachTitle).then(console.log);
*/
/***********EXO 9*********/
/*
var json = process.argv[2];

function parsePromised(x)
{
  return new Promise(function (resolve, reject)
  {
    try
    {
      resolve(JSON.parse(x));
    }
    catch (e)
    {
      reject(e.message);
    }
  })

}

parsePromised(json).then(null, console.log);
*/
/***********EXO 10*********/
/*
function alwaysThrows()
{
  throw new Error('OH NOES');
}

function iterate(num)
{
  console.log(num+1);
  return num+1;
}

var promise = new Promise(function (resolve, reject)
  {
    resolve(iterate(0));

  })
promise.then(iterate)
        .then(iterate)
        .then(iterate)
        .then(iterate)
        .then(alwaysThrows)
        .then(iterate)
        .then(iterate)
        .then(iterate)
        .then(iterate)
        .then(null, (e) => console.log(e.message))
*/
/***********EXO 11*********/

function all(x, y)
{
  let promise3 = new Promise (function (resolve) {

    var counter = 0;
    var values = [];


    var mot1 = x.then((mot1) => {
      counter++;
      values[0] = mot1;
      if (counter == 2)
      {
        resolve(values);
      }
    })

    var mot2 = y.then((mot2) => {
      counter++;
      values[1] = mot2;
      if (counter == 2)
      {
        resolve(values);
      }
    })

  })

  return promise3;
}

all(getPromise1(), getPromise2()).then(console.log);

