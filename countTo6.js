/*************FONCTIONS FLECHEES**************/
/*
var inputs = process.argv.slice(2);
var result = inputs.map((value) => value.substr(0,1))
                    .reduce((a,b) => a.concat(b));
console.log(result);

/*************THIS**************/
/*
var foot = {
    kick: function () {
        this.yelp = "Ouch!";
        setImmediate(() => console.log(this.yelp));
    }
};
foot.kick();
*/
/*************DESTRUCTURING**************/
/*
let userArray = process.argv.slice(2);
let newArray = {};
[,newArray.username, newArray.email] = userArray;

console.log(newArray);
*/
/*************SPREAD**************/
/*
const numbers = process.argv.slice(2);

console.log(`The minimum of ${numbers} is ${Math.min(...numbers)}.`);
*/

/*************TAGGED TEMPLATE STRINGS**************/

console.log(html`<b>${process.argv[2]} says</b>: "${process.argv[3]}"`);


function html(strings, ...interpolations) {

    let phrase = strings[0];

    for (let i=0; i<strings.length-1; i++)
    {
        phrase += htmlEntities(interpolations[i]) + strings[i+1];
    }
    return phrase;
}

function htmlEntities(val)
{
    return val.replace(/&/g, `&amp;`)
            .replace(/'/g, `&apos;`)
            .replace(/"/g, `&quot;`)
            .replace(/</g, `&lt;`)
            .replace(/>/g, `&gt;`);
}