class Calculatrice
{
    constructor(num1, num2)
    {
        this.number1 = this.isANumber = num1;
        this.number2 = this.isANumber = num2;
    }

    set isANumber(num)
    {
        if (Number.isInteger(parseInt(num)))
        {
            return num;
        }
        else
        {
            alert('Merci de saisir un nombre valide');
        }
    }

    additionner()
    {
        return this.number1+this.number2;        
    } 
    
    soustraire()
    {
        return this.number1 - this.number2;
    }
    
    multiplier()
    {
        return this.number1 * this.number2;
    }
    
    diviser()
    {
        return this.number1 * this.number2;
    }
    
    modulo()
    {
        return this.number1 % this.number2;
    }
}
 

var nombre1 = document.getElementById('input1');
var nombre2 = document.getElementById('input2');
var operation = document.getElementById('operation');
var action = document.getElementById('submit');

action.addEventListener('click', function(){
    let value1 = parseInt(nombre1.value);
    let value2 = parseInt(nombre2.value);
    
    let calc = new Calculatrice(value1, value2);

    switch(operation.value)
    {
        case '+': result = calc.additionner();
            break;
        case '-': result = calc.soustraire();
            break;
        case '*': result = calc.diviser();
            break;
        case '%': result = calc.modulo();
            break;
    }
    console.log(result);
})