class Creature
{
    constructor(name, age, strength)
    {
        this.name = name;
        this.age = age;
        this.strength = strength;
    }

    saySomething()
    {
        console.log('Je parle');
    }

    identity()
    {
        console.log(`name: ${this.name}`);
        console.log(`age: ${this.age} ans`);
        console.log(`strength: ${this.strength}`);
    }

}