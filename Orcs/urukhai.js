class Urukhai extends Orc
{
    constructor(name, age, strength)
    {
        super(name, age, strength);
        this.strength += 10;
    }

    kill(someone)
    {
        console.log(this.name+'  a tué '+someone.name);
    }
}