class ChiefOrc extends Orc
{
    constructor(name, age, strength, isChief)
    {
        super(name, age, strength);
        this.isChief = isChief;
        this.strength += 2;
    }

    giveOrder(someone)
    {
        console.log(this.name+' donne un ordre à '+someone.name);
    }
}