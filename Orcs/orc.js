class Orc extends Creature
{
    constructor(name, age, strength)
    {
        super(name, age, strength);
    }

    scream(words)
    {
        let toUpper = words.toUpperCase();
        console.log(toUpper);
    }

    bite(someone)
    {
        console.log(this.name+' frappe '+someone.name);
    }
}